package controller

import (
	"begorm/app/model"
	"begorm/app/utils"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
)

func (inDB InDB) CreateAccount(c *gin.Context) {
	accountModel := model.InDB{
		DB: inDB.DB,
	}
	var account model.Account
	if err := c.Bind(&account); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	pass, err := utils.HashGenerator(account.Password)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	account.Password = pass
	flag, err := accountModel.InsertNewAccount(account)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func (inDB InDB) GetAccount(c *gin.Context) {
	idAccount := c.MustGet("account_number").(int)
	accountModel := model.InDB{
		DB: inDB.DB,
	}
	flag, err, trx, acc := accountModel.GetAccountDetail(idAccount)
	if err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusInternalServerError)
		return
	}
	if flag {
		utils.WrapAPIData(c, map[string]interface{}{
			"account":     acc,
			"transaction": trx,
		}, http.StatusOK, "success")
		return
	}
}

func (inDB InDB) Transfer(c *gin.Context) {
	accountModel := model.InDB{
		DB: inDB.DB,
	}
	var transaction model.Transaction
	if err := c.Bind(&transaction); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	flag, err := accountModel.Transfer(transaction)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func (inDB InDB) Withdraw(c *gin.Context) {
	accountModel := model.InDB{
		DB: inDB.DB,
	}
	var transaction model.Transaction
	if err := c.Bind(&transaction); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	flag, err := accountModel.Withdraw(transaction)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func (inDB InDB) Deposit(c *gin.Context) {
	accountModel := model.InDB{
		DB: inDB.DB,
	}
	var transaction model.Transaction
	if err := c.Bind(&transaction); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	flag, err := accountModel.Deposit(transaction)
	if flag {
		utils.WrapAPISuccess(c, "success", http.StatusOK)
		return
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
}

func (inDB InDB) Login(c *gin.Context) {
	accountModel := model.InDB{
		DB: inDB.DB,
	}
	var auth model.Auth
	if err := c.Bind(&auth); err != nil {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
		return
	}
	log.Println("LOGIN")
	flag, err, token := accountModel.Login(auth)
	if flag {
		//c.SetCookie("value", token, 3600, "", "", false, true)
		c.Header("Authorization", token)
		utils.WrapAPIData(c, map[string]interface{}{
			"token": token,
		}, http.StatusOK, "success")
	} else {
		utils.WrapAPIError(c, err.Error(), http.StatusBadRequest)
	}
}
