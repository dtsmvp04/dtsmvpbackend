package main

import (
	"begorm/app/config"
	"begorm/app/controller"
	"begorm/app/middleware"
	"fmt"
	"time"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func main() {
	db := config.DBInit()
	inDB := &controller.InDB{DB: db}

	router := gin.Default()
	//router.Use(cors.Default())
	router.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"},
		AllowMethods:     []string{"PUT", "PATCH", "POST", "GET", "OPTIONS"},
		AllowHeaders:     []string{"Content-Type", "Authorization"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowOriginFunc: func(origin string) bool {
			return origin == "https://github.com"
		},
		MaxAge: 12 * time.Hour,
	}))
	//router.GET("/", inDB.CreateAccount)
	router.POST("/api/v1/account/add", inDB.CreateAccount)
	router.POST("/api/v1/login", inDB.Login)
	router.GET("/api/v1/account", middleware.Auth, inDB.GetAccount)
	router.POST("/api/v1/transfer", middleware.Auth, inDB.Transfer)
	router.POST("/api/v1/withdraw", middleware.Auth, inDB.Withdraw)
	router.POST("/api/v1/deposit", middleware.Auth, inDB.Deposit)
	fmt.Println("Running On PORT 8081")
	router.Run(":8081")
}
